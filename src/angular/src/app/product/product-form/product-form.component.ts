import {Component, Input} from "@angular/core";
import {FormGroup, FormBuilder} from "@angular/forms";
import {ExamService} from "../../client/exam.service";
import {Router} from "@angular/router";

@Component({
  selector: 'product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent {

  @Input() productForm?: FormGroup;

  constructor(private readonly _router: Router,
              private readonly _formBuilder: FormBuilder,
              private readonly _exameService: ExamService,) {
    this.productForm = this._buildProductForm()
  }

  private _buildProductForm(): FormGroup {
    return this._formBuilder.group({
      description: [],
      price: [],
    });
  }

  async createProduct() {
    const formData = this.productForm.value;

    const product = await this._exameService.addProduct({
      description: formData.description,
      price: formData.price,
    }).toPromise();

    this._router.navigate(['product/' + product.id]);
  }
}
