import {Component, OnInit} from "@angular/core";
import {ExamService} from "../../client/exam.service";
import {Product} from "../../model/product";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'product-show',
  templateUrl: './product-show.component.html',
  styleUrls: ['./product-show.component.scss']
})
export class ProductShowComponent implements OnInit {

  private _productId: number;
  product: Product;

  constructor(private readonly _activatedRoute: ActivatedRoute, private readonly _exameService: ExamService) {
  }

  async ngOnInit() {
    this._productId = this._resolveProductId();
    this.product = await this.fetchProduct();
  }

  async fetchProduct() {
    return await this._exameService.getProduct(this._productId).toPromise();
  }

  private _resolveProductId(): number {
    const id = Number(this._activatedRoute.snapshot.params.id);

    if (!id) {
      throw Error('Id do produto não informado');
    }

    return id;
  }
}
