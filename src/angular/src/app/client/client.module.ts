import {ModuleWithProviders, NgModule} from "@angular/core";
import {ExamService} from "./exam.service";

@NgModule({
  imports: [],
  entryComponents: [],
  declarations: [],
  exports: [],
})
export class ClientModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ClientModule,
      providers: [
        ExamService,
      ],
    };
  }
}
