import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Product} from "../model/product";
import {Observable} from "rxjs";
import {HttpResponse} from "@angular/common/http";
import {HttpEvent} from "@angular/common/http";
import {HttpHeaders} from "@angular/common/http";

//Implementei um service para fazer a requisição para o serviço, porém o utilio o swagger para gerar um client,
//mas não deu tempo de fazer a parte de configuração o swagger gerar o client automaticamente
// Swagger gerar mais ou menos com essa estrutura
@Injectable()
export class ExamService {

  protected basePath = 'http://localhost:8080/api';
  public defaultHeaders = new HttpHeaders();

  constructor(protected httpClient: HttpClient) {
  }

  public addProduct(body: Product, observe?: 'body', reportProgress?: boolean): Observable<Product>;
  public addProduct(body: Product, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Product>>;
  public addProduct(body: Product, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Product>>;
  public addProduct(body: Product, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
      if (body === null || body === undefined) {
      throw new Error('Corpo da requisição obrigatório');
    }

    let headers = this.defaultHeaders;
    headers = headers.set("Accept", 'application/json');
    headers = headers.set("Content-Type", 'application/json');

    return this.httpClient.post<Product>(
      `${this.basePath}/product/new`,
      body,
      {
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

  public getProduct(productId: number, observe?: 'body', reportProgress?: boolean): Observable<Product>;
  public getProduct(productId: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Product>>;
  public getProduct(productId: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Product>>;
  public getProduct(productId: number, observe: any = 'body', reportProgress: boolean = false): Observable<any> {
    if (productId === null || productId === undefined) {
      throw new Error('Id do producto é obrigatório');
    }

    let headers = this.defaultHeaders;
    headers = headers.set("Accept", 'application/json');
    headers = headers.set("Content-Type", 'application/json');

    return this.httpClient.get<Product>(
      `${this.basePath}/product/${productId}`,
      {
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }

}
