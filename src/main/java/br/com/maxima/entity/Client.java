package br.com.maxima.entity;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class Client extends BaseEntity {

    @NotNull
    private String email;

    @NotNull
    private String tradingName;
}
