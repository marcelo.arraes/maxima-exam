package br.com.maxima.service;

import br.com.maxima.entity.BaseEntity;
import br.com.maxima.exception.NotFoundException;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public abstract class AbstractCrudService<T extends BaseEntity> {

    protected final JpaRepository<T, Long> dao;

    protected AbstractCrudService(JpaRepository<T, Long> dao) {
        this.dao = dao;
    }

    public T findById(final Long id) throws NotFoundException {
        Optional<T> optionalEntity = dao.findById(id);

        if (!optionalEntity.isPresent()) {
            throw new NotFoundException();
        }

        return optionalEntity.get();
    }

    public List<T> findAll() {
        return dao.findAll();
    }

    @Transactional
    public T create(T entity) {
        if (entity.getId() != null) {
            entity.setId(null);
        }

        return dao.save(entity);
    }

    @Transactional
    public T update(Long id, T entity) throws NotFoundException {
        if (!dao.existsById(id)) {
            throw new NotFoundException();
        }

        entity.setId(id);
        return dao.save(entity);
    }

    public void delete(Long id) throws NotFoundException {
        T entity = findById(id);
        dao.delete(entity);
    }
}
