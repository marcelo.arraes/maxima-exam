package br.com.maxima.controller;

import br.com.maxima.entity.Product;
import br.com.maxima.exception.NotFoundException;
import br.com.maxima.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ResponseEntity<Product> create(@RequestBody final Product product) {
        return ResponseEntity.ok(productService.create(product));
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.GET)
    public ResponseEntity<Product> findById(@PathVariable final Long productId) {
        try {
            final Product product = productService.findById(productId);
            return ResponseEntity.ok(product);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
